/**
 * Created by bhaskar_q on 27/10/16.
 */
class CheckPrice{
	run(intentData, sessionData){
		if(intentData.calculations.response.calculationDetails.totalPurchaseAmount>0){
			return new Intent("next", intentData);
		}
		else{
			return new Intent("free_ticket", intentData);
		}

	}
}