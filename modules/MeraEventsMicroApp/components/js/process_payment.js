class ProcessPayment{
	run(intentData, sessionData){
		let fareDetails = [];
		_.each(intentData.calculations.response.calculationDetails.ticketsData, function(item,key){
			fareDetails.push({
				ticketTitle:item.ticketName,
				quantity:item.selectedQuantity,
				totalAmount:item.totalAmount,
				currency:item.currencyCode
			});
		});
		intentData.calculations.fareDetails = fareDetails;
		let extraCharge = [];
		_.each(intentData.calculations.response.calculationDetails.extraCharge, function(item,key){
			extraCharge.push({
				detail:item.label,
				charge:item.totalAmount,
				currency:item.currencyCode
			});
		});
		intentData.calculations.extraCharge = extraCharge;
		let totalTaxDetails = [];
		_.each(intentData.calculations.response.calculationDetails.totalTaxDetails, function(item,key){
			totalTaxDetails.push({
				label:item.label,
				tax:item.taxAmount,
				percent:item.value
			});
		});
		intentData.calculations.totalTaxDetails = totalTaxDetails;
		console.log("IntentData", intentData);
		return new Intent("next", intentData);
	}
}