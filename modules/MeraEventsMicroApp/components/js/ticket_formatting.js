class TicketFormatting{
	run(intentData, sessionData){
		if(intentData.paytm_wallet_pay_res && intentData.paytm_wallet_pay_res.STATUS=="TXN_FAILURE"){
			intentData.paytm_error = intentData.paytm_wallet_pay_res.RESPMSG;
			return new Intent("back_to_pay", intentData);
		}
		else{
			var data = intentData;
			var tickets = [];
			_.each(data.booked_ticket_details.response.eventSignupDetailData.ticketDetails , function(item){
				item.currency = data.booked_ticket_details.response.eventSignupDetailData.eventsignupDetails.currencyCode;
				tickets.push(item);
			});
            intentData.tickets = tickets;
			return new Intent("next", intentData);
		}
	}
}