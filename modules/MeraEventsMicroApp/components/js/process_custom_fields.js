class ProcessCustomFields{
	run(intentData, sessionData){
        console.log("All ticket Name", intentData.tickets);
		var savedFieldGroups = [];
		_.each(sessionData.commonFieldGroups, function(item){
			savedFieldGroups.push(item);
		});

		var customFields = intentData.custom_fields.response.customFields;
		var ticketRegistrations = [];
		_.each(customFields, function (item, fieldsIndex) {
			_.each(item.formFields, function (_item, index) {
				if(intentData.custom_fields.response.customFieldValues[_item.id]){
					item.formFields[index]["values"] = intentData.custom_fields.response.customFieldValues[_item.id];

				}
				if(intentData.input_userDetails){
					if(fieldsIndex==0){
						if(item.formFields[index].commonfieldid==1){
							item.formFields[index].field_value = intentData.input_userDetails.name || "";
							if(intentData.input_userDetails.name){
								item.formFields[index].valid = true;
							}
						}
						else if(item.formFields[index].commonfieldid==2){
							item.formFields[index].field_value = intentData.input_userDetails.email || "";
							if(intentData.input_userDetails.email) {
								item.formFields[index].valid = true;
							}
						}
						else if(item.formFields[index].commonfieldid==3){
							item.formFields[index].field_value = intentData.input_userDetails.phone || "";
							if(intentData.input_userDetails.phone) {
								item.formFields[index].valid = true;
							}
						}
					}
				}
				else{
					if(fieldsIndex < savedFieldGroups.length){
						var prefilledValue = savedFieldGroups[fieldsIndex];
						if(item.formFields[index].commonfieldid){
							item.formFields[index].field_value = prefilledValue[item.formFields[index].commonfieldid];
							item.formFields[index].valid = true;
						}
					}
				}
                if(!item.formFields[index].field_value){
                    item.formFields[index].field_value = item.formFields[index].defaultValue;
                    if(item.formFields[index].values){
                        _.each(item.formFields[index].values, function (_valueItem) {
                            if(_valueItem.isdefault){
                                item.formFields[index].field_value = _valueItem.value;
							}
                        });
					}
				}
			});
			ticketRegistrations.push({
				index:fieldsIndex+1,
				ticket:_.find(intentData.event_details.tickets, function(ticket){return ticket.id=item.id}),
				fields:item.formFields
			});
		});
		delete intentData.custom_fields;
		intentData.ticketRegistrations = ticketRegistrations;
		intentData.savedFieldGroups = savedFieldGroups;
		console.log("Meraevents fields", JSON.stringify(intentData.ticketRegistrations));
		return new Intent("next", intentData);
	}
}