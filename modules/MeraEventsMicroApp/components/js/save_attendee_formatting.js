class SaveAttendeeFormatting{
	run(intentData, sessionData){
		intentData.promo_code = "";
		var formData = {
			orderId:intentData.book_now.response.orderId,
			isEmailEnable:"TRUE",
			isSmsEnable:"TRUE",
			eventId:intentData.eventId,
			ticketArr:intentData.ticketArray,
			paymentGatewayId:"6"
		};
		_.each(intentData.ticketRegistrations, function(item){
			_.each(item.fields, function(field){
				formData[field.fieldnameid] = field.field_value;
			});
		});
		var commonFieldGroups = sessionData.commonFieldGroups || {};
		_.each(intentData.ticketRegistrations, function(item){
			var commonField = _.find(item.fields, function(field){
				if(field.commonfieldid){
					return field.commonfieldid==1;
				}
				return false;
			});
			if(commonField){
				var group = commonFieldGroups[commonField.field_value]||{};
				_.each(item.fields, function (field,index) {
					if(field.commonfieldid!=0){
						group[field.commonfieldid] = field.field_value;
					}
				});
				commonFieldGroups[commonField.field_value] = group;
			}
		});
		sessionData.commonFieldGroups = commonFieldGroups;
		intentData.commonFieldGroups = JSON.stringify(commonFieldGroups);
		intentData.save_attendee_data = formData;
		return new Intent("next", intentData);
	}

}