class ProcessEventRequest{
	run(intentData, sessionData){
		var date = new Date().getTime();
		var valid_tickets = [];
		intentData.promo_code = intentData.promo_code || '';
		var startDate = intentData.event_details.response.details.startDate;
		intentData.event_details.response.details.startDate = startDate.replace(" ","T");
		var webDesc = intentData.event_details.response.details.description.replace(/[\u2028]/g,"");
		var lines = webDesc.split("\n");
		var content = "";
		_.each(lines, function (line) {
			content = content+"<style>body{margin:0px;font-size: 14dpi;}</style>"+ "<p>"+line+"</p>";
		});
		intentData.event_details.response.details.description = content;
		var endDate = intentData.event_details.response.details.endDate;
		intentData.event_details.response.details.endDate = endDate.replace(" ","T");

		for(let ticket of intentData.event_details.response.ticketDetails){
			ticket.startDate = ticket.startDate.replace(" ","T");
			ticket.endDate = ticket.endDate.replace(" ","T");
			if(ticket.displayStatus && ticket.status){
				ticket.required_quantity = 0;
				valid_tickets.push(ticket);
			}
		}
        intentData.total_tickets = 0;
		intentData.event_details.valid_tickets = _.sortBy(valid_tickets, function(item){
			return item.soldout||item.upcomingTicket||item.pastTicket;
			console.log("check soldout",intentData.event_details.valid_tickets );
		});
		//intentData.event_details.valid_tickets = valid_tickets;
		return new Intent("next", intentData);
	}
}