/**
 * Created by bhaskar_q on 16/11/16.
 */
class PaytmPreProcessingOrder_SC{
	run(intentData, sessionData){
		intentData.checksum_params = {
			REQUEST_TYPE:"PAYTM_EXPRESS",
			ORDER_ID:intentData.saveAttendee.eventSignupId,
			CUST_ID:intentData.saveAttendee.userDetails.email,
			TXN_AMOUNT:intentData.totalBillable,
			MSISDN:sessionData.paytm_number,
			PAYMENT_DETAILS:"200bf5a043d401d36ea9daf0f4bed28d2d771a8e36a634b445793b2aed2621f6",
			AUTH_MODE:"USRPWD",
			PAYMENT_TYPE_ID:"NB",
			ADD_MONEY:intentData.totalBillable-intentData.paytm_balance,
			SSO_TOKEN:sessionData.paytm_token.TXN_TOKEN,
			PAYTM_TOKEN:sessionData.paytm_token.PAYTM_TOKEN,
			MID:sessionData.paytm_MID,
			CHANNEL_ID:"WAP",
			INDUSTRY_TYPE_ID:"Retail104",
			THEME:"Merchant",
			WEBSITE:"Meraevents",
			BANK_CODE:intentData.selected_bank.code,
            CALLBACK_URL:"https://production.appsfly.io/paytm/post-params"
		};
		return new Intent("next", intentData);
	}
}

