/**
 * Created by bhaskar_q on 16/11/16.
 */
class PaytmPreProcessingOrder_SC{
	run(intentData, sessionData){
		intentData.checksum_params = {
			REQUEST_TYPE:"PAYTM_EXPRESS",
			ORDER_ID:intentData.saveAttendee.eventSignupId,
			CUST_ID:intentData.saveAttendee.userDetails.id,
			TXN_AMOUNT:intentData.totalBillable,
			MSISDN:sessionData.paytm_number,
			EMAIL:intentData.saveAttendee.userDetails.email,
			PAYMENT_DETAILS:intentData.card_token.TOKEN,
			AUTH_MODE:"3D",
			PAYMENT_TYPE_ID:"CC",
			ADD_MONEY:1,
			SSO_TOKEN:sessionData.paytm_token.TXN_TOKEN,
			PAYTM_TOKEN:sessionData.paytm_token.PAYTM_TOKEN,
			MID:sessionData.paytm_MID,
			CHANNEL_ID:"WAP",
			INDUSTRY_TYPE_ID:"Retail104",
			THEME:"Merchant",
			WEBSITE:"Meraevents",
			CALLBACK_URL:"https://production.appsfly.io/paytm/post-params"
		};
		return new Intent("next", intentData);
	}
}

