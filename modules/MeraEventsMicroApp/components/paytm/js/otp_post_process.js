/**
 * Created by bhaskar_q on 27/10/16.
 */

class OtpPostProcess{
	run(intentData, sessionData){
		sessionData.paytm_state  = intentData.paytm_otp_res.state;
		return new Intent("next", intentData);
	}
}