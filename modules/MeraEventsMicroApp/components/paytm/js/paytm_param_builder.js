/**
 * Created by bhaskar_q on 27/10/16.
 */
class PaytmNBParams{
	run(intentData, sessionData){
		var data = intentData;
		var body = null;
		_.each(data.checksum, function(item, key){
			if(key=="CHECKSUM"){
				if(!body){
					body = "CHECKSUMHASH="+encodeURIComponent(item);
				}
				else{
					body = body+"&"+"CHECKSUMHASH="+encodeURIComponent(item);
				}
			}
			else{
				if(!body){
					body = key+"="+item;
				}
				else{
					body = body+"&"+key+"="+item;
				}
			}

		});
		intentData.open_payment_window=true;
		intentData.payment_window_params = {
			data:body
		};
		return new Intent("next", intentData);
	}
}