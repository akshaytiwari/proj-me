/**
 * Created by bhaskar_q on 16/11/16.
 */
class PaytmPreProcessingOrder_SC{
	run(intentData, sessionData){
		intentData.checksum_params = {
			REQUEST_TYPE:"PAYTM_EXPRESS",
			ORDER_ID:intentData.saveAttendee.eventSignupId,
			CUST_ID:intentData.saveAttendee.userDetails.id,
			TXN_AMOUNT:intentData.totalBillable,
			MSISDN:sessionData.paytm_number,
			EMAIL:intentData.saveAttendee.userDetails.email,
			AUTH_MODE:"USRPWD",
			PAYMENT_TYPE_ID:"NB",
			BANK_CODE:intentData.selected_bank.BANK_CODE,
			MID:sessionData.paytm_MID,
			CHANNEL_ID:"WEB",
			INDUSTRY_TYPE_ID:"Retail104",
			THEME:"Merchant",
			WEBSITE:"Meraevents",
            CALLBACK_URL:"https://production.appsfly.io/paytm/post-params"
		};

		return new Intent("next", intentData);
	}
}

