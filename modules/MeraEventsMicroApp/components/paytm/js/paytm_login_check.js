/**
 * Created by bhaskar_q on 27/10/16.
 */
class PaytmLoginCheck{
	run(intentData, sessionData){
		sessionData.paytm_MID = "MeraEE96391813278456";
		intentData.use_wallet = true;
		if(sessionData.paytm_token){
			intentData.paytm_number = sessionData.paytm_number;
			return new Intent("paytm_pay", intentData);
		}
		else{
			return new Intent("next", intentData);
		}
	}
}